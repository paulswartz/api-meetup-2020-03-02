---
title: MBTA's public API 2020-03
revealOptions:
    defaultTiming: 20
---

# MBTA V3 API

[api-v3.mbta.com](https://api-v3.mbta.com/)

<small>2020-03-25</small>

Note: thanks everyone for joining us today! I'm Paul Swartz, lead architect
in MBTA's Customer Technology Department. Today I'll be giving a quick
overview of our public API, and some of the features in Elixir/Erlang/OTP
which help us build and scale it.

---
![](assets/paul.jpg)

Note: I started in MBTA's Customer Technology department in 2016, coming from
a background building Python and Ruby applications. Our first project was a
complete rebuild of MBTA.com, the Authority's homepage.

---
![](assets/dotcom.png)

Note: We had a unique opportunity to start from scratch, technically. We knew
that we wanted it to be data-driven, so that our riders could get real-time
information about their ride. We needed it to scale to all our riders, and we
needed it to be cost-effective (we're a public agency after all).

I'd worked a bit with Elixir at my previous job, and thought that it might be
a good fit for what we needed: functional, great concurrency support,
friendly syntax, a solid web framework in Phoenix, and plenty of community on
both the Elixir and Erlang sides. Elixir was also starting to get traction
with some of the agencies in the Boston area. We did some initial prototypes
in Python and Ruby as well, but we couldn't ignore the speed and ease of
development we got from Elixir.

---
![](https://miro.medium.com/max/992/1*41JcuxYj8CyOpTE0S50xSw.png)<br>
[api-v3.mbta.com](https://api-v3.mbta.com)
<br>
[gh:mbta/api](https://github.com/mbta/api)

Note: We started development on the API in 2016 along with the website. The
API provides the schedule and prediction information as a JSON:API, which the
website consumes. We opened up registration publicly at the end of 2017, and
open-sourced the code under the MIT license at the start of 2019.

---

# 676
million requests

Note: In February. Weekday rush hour, we see ~400
requests/second, with peaks up to 600rps.

---

# 2 ms

Note: median response time. 99% is currently under 300ms.

---

# Where does the data come from?

----

## GTFS
.zip[.csv]

## GTFS-RealTime
Protobuf | JSON

Note: my colleagues will be talking a little more about these, but they're
both standards in the transit world, used by agencies like us as well as
consumers such as Google and Transit app. GTFS is for static information such
as routes, stops, and timetables. GTFS-RealTime is for predictions, vehicle
locations, and service alerts.

Internally, we build on the :zip and :gpb modules from Erlang, as well as the
ExCSV and Exprotobuf librarries in Elixir. This is a great feature of
the BEAM ecosystem: we get to benefit from libraries provided by both
communities interchangably.

---

# Elixir?

Note: So how does all of this help the MBTA? Why Elixir and not some other language?

----

# Functional
### (just enough)

- immutability
- processes / OTP
- optional typing

Note: Functional programming is great, but not every developer or team wants
to go all the way to Haskell or Elm. Elixir gives us most of what we need to
be effective.

Since the built-in data structures (lists, maps, tuples) are immutable, we
avoid the issues we see in Python or Ruby where a function modifies an
argument, or an object modifies itself behind the scenes. Immutability makes
it clearer to developers what's changing, and makes it easier for us to write
unit and property tests around the functionality.

Processes and OTP let us model separate parts of the system as if they were running
by themselves. For example, in the API each HTTP endpoint we fetch from gets
its own process, responsible for maintaining state like cache timeouts. If
one of them crashes, that doesn't affect any of the other fetches. Each
incoming HTTP request from a client gets its own process (thanks to Cowboy
and Phoenix), and none of them affect the others.

Optional typing gives us some of the benefits of a stricter language, but in
practice they serve more as documentation than something catching bugs at
compile-time.

That's the language itself: what about the rest of the BEAM ecosystem?

----

# Mnesia

"A distributed telecommunications DBMS"

Note: The v3 API is heavily based on Mnesia, "a distributed
telecommunications DBMS". What does that mean?

----

## in-memory database

ETS tables, but with indexes and transactions

Note: For us, it's a fast, in-memory database. All of the schedule and
prediction data is loaded into Mnesia tables for querying. We have some
additional ETS tables for post-processed data as well.

In practice, what this means is each API instance is self-contained. If we
want to serve more queries, we can spin up new instances without worrying
about overloading a downstream database. We also avoid any additional network
traffic for queries, as they're all served from memory.

One downside from the Elixir side is that Mnesia (and ETS) store their data
as records (tagged tuples), whereas Elixir mostly uses structs (tagged
maps). There's some conversion overhead going between them.

----

```elixir
iex> Schedule.filter_by(%{
  route_id: ["Red"],
  stop_id: ["place-brdwy"]
})

[
  %Schedule{
    route_id: "Red",
    stop_id: "place-brdwy",
    arrival_time: 43_200 # seconds after midnight
    ...
  },
  ...
]
```

Note: in Elixir, we provide a simple map-based query language, similar to
what clients can use via JSON:API.  This is an example looking for Red line
schedules at Broadway Station, and returning a schedule which arrives there
at noon. In Mnesia, that's translated to...

----

```elixir
# in Mnesia
iex> :mnesia.index_match_object(
  Schedule,
  {:schedule, "Red", "place-brdwy", :_, ...},
  :route_id, # index
  :dirty # lock type
)

[
  {:schedule, "Red", "place-brdwy", 43_200, ...},
  ...
]
```

Note: ...a match query, which returns records. The `:_` part of the match
says "we'll accept anything in the arrival_time field". We then translate the
records back into structs that the rest of the Elixir code works with.

----

# Registry

```elixir
# sub
{:ok, _} = Registry.register(RegistryName, topic, sub_argument)

# pub
Registry.dispatch(RegistryName, topic, fn entries ->
  for {pid, sub_argument} <- entries do
    send(pid, {:event, topic, data, sub_argument})
  end
end)
```

Note: Another Elixir feature we rely on heavily is the Registry, added in
Elixir 1.4: "scalable key-value process storage". We use it for pub/sub:
different parts of the system can subscribe to a topic and be notified when
the data change. This is especially important for streaming data such as
vehicle locations or prediction updates, which clients want as fast as
possible.

When a client asks for a streaming connection, we create a registry topic
based on their query: this also means that multiple connections asking for
the same query can share a process. That process can then register for the
upstream topics it cares about, and notify the downstream consumers as soon
as the new data is ready. At any point, we have around 1000 processes running
per server and Registry doesn't break a sweat.

----

# Performance
tips & tricks

Note: The BEAM isn't necessarily the right choice if you need straight CPU
power, since it's running in a virtual machine. But if we work with the BEAM,
we can get great performance while still maintaining the other benefits we
talked about earlier. This allows us to use smaller instances and save money
on hosting costs.

----

### Refc vs. heap binaries

```elixir
<< first_five_bytes_slice::binary-5,
   _rest::binary>> = <large refc binary>
five_five_bytes_on_heap = :binary.copy(first_five_bytes_slice)
```

Note: If you're used to other garbage-collected languages, there are some
important differences to be aware of. Instead of having a global
garbage-collection step, each process is garbage-collected seperatetely. Data
is usually passed by copying so each process owns its data, allowing each
process to be garbage-collected separately. However, larger binaries
(>64bytes) are stored off-head and passed by reference, saving lots of time
relative to copying between processes. However, it also means that multiple
processes can maintain a reference to it (or to a slice) keeping it around
longer than expected. If you don't want to keep all of that binary around,
you'll want to create a copy so that the larger binary can be garbage
collected. This is especially important if you're storing the data in
long-term storage, such as an Mnesia or ETS table, which can keep a heap
binary around indefinitely.

----

### Hibernation

```elixir
{:reply, :ok, :state, :hibernate}
{:noreply, :state, :hibernate}
:erlang.hibernate/3
:proc_lib.hibernate/3
```

Note: Hiberation is a useful feature that the VM provides. It runs a GC
sweep and puts the process into a low-memory state. This can be very helpful
if the process does not receive messages often, say for example if it's
owning an ETS table or only doing occasionally processing. We use it for the
processes which maintain the schedule tables, as well as for long-running
client connections.

----

### Profiling

```elixir
{time_usec, result} = :timer.tc(Module, :fun, [argument, ...]
```

Note: None of these features matter if they aren't actually improving
performance, so how do you know if they are? Erlang's built-in :timer module
is a great place to start. It calls your function and returns the number of
microseconds it took to run. I'm still often surprised by what parts of my
code are slow, and this is an easy way to check your assumptions.

----

### EFlame
![](https://camo.githubusercontent.com/e665c63c50d4f8bfbb04e25971032689c97c92e4/687474703a2f2f692e696d6775722e636f6d2f584944416364332e706e67)
```elixir
{:eflame, "~> 1.0"} # in Mix.exs

eflame:apply(Module, :fun, [argument])
```

Note: once you need to check more complicated functions, that's where a
flamechart comes in. Built on Erlang's trace module, EFlame creates a flame
chart for all of the functions that are being called. Taller flames are
deeper call stacks, and wider flames run for longer. It's a great visual
overview of what your code is doing, and can help drill back down into more
specific functions.

----

### in Production

```
# Mix.exs
{:ehmon, github: "mbta/ehmon", branch: "master", only: :prod}

# config/prod.exs
config :ehmon, :report_mf, {:ehmon, :info_report}
```

Note: We do nearly all our monitoring via Splunk, and Heroku's ehmon lets us
log metrics like memory usage, run queue, and scheduler percentage along with
the rest of our data. These are very coarse measures, but help us keep our
performance on track as we develop new features.

---

# Lessons learned

- functional + processes
- built-ins + community packages
- measure for performance (locally & production)

Note: So, what have we learned at the MBTA, after using Elixir for our API
for nearly 4 years? 1) Functional programming plus the OTP process model are a
great combination: they let each process focus on one thing, without worrying
much about whether another process is going to step on it.

2) There's a great community around both Erlang and Elixir, and we benefit from
both communities. Since packages from either can be used, we get the best of both worlds.

and 3) While the performance is good out of the box, by paying attention to things
like memory and scheduler usage, we can scale up to millions of requests.

---

# Questions?

* @paulswartz ([twitter](https://twitter.com/paulswartz) / [github](https://github.com/paulswartz))
* pswartz@mbta.com
* [mbta.com/developers](https://www.mbta.com/developers)

---

# Thanks!
